import { Component, h } from "@stencil/core";
@Component({
  tag: "component-logo"
})
export class ComponentLogo {
  render() {
    return (
      <div class="logo">
        <img alt="Superior Maintenance Logo" src="/assets/logo.svg"></img>
      </div>
    );
  }
}
