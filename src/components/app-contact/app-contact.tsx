import { Component, h } from "@stencil/core";
import { renderHeading } from "../../helpers/utils";

@Component({
  tag: "app-contact",
  styleUrl: "app-contact.css"
})
export class AppContact {
  render() {
    return [
      renderHeading(),
      <ion-content class="ion-no-padding">
        <ion-grid>
          <ion-row>
            <ion-col size="12" size-lg="4" push-lg="8">
              <genie-card no-hover class="no-grow" heading="Contact Us">
                <ul>
                  <li>
                    Phone:
                    <br /> <a href="tel:7153794872">715-379-4872</a>
                  </li>
                  <li>
                    Email:{" "}
                    <a href="mailto:superiormaintenance.jepperson@gmail.com">
										<span class="nowrap">superiormaintenance</span>.<span class="nowrap">jepperson</span>@<span class="nowrap">gmail</span>.com
                    </a>
                  </li>
                </ul>
              </genie-card>
            </ion-col>
            <ion-col size="12" size-lg="8" pull-lg="4">
              <genie-card no-hover class="contact-form" heading="Contact Form">
                <form action="https://formspree.io/xwkqqnpa" method="POST">
                  <ion-item>
                    <ion-label color="light" position="floating">First Name</ion-label>
                    <ion-input name="firstname" color="light" type="text"></ion-input>
                  </ion-item>
									<ion-item>
                    <ion-label color="light" position="floating">Last Name</ion-label>
                    <ion-input name="lastname" color="light" type="text"></ion-input>
                  </ion-item>
									<ion-item>
                    <ion-label color="light" position="floating">Email Address</ion-label>
                    <ion-input name="email" color="light" type="text" inputmode="email" required></ion-input>
                  </ion-item>
									<ion-item>
                    <ion-label color="light" position="floating">Message</ion-label>
                    <ion-textarea name="message" color="light"></ion-textarea>
                  </ion-item>
                  <ion-button type="submit" color="primary" expand="block">
                    Submit
                  </ion-button>
                </form>
              </genie-card>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
