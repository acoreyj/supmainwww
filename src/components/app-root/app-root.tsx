import { Component, h } from "@stencil/core";

@Component({
  tag: "app-root",
  styleUrl: "app-root.css"
})
export class AppRoot {

	pageLoaded = false;

  private async onRouteWillChange() {
		if (this.pageLoaded) {
			const loading = document.createElement('ion-loading');
			loading.duration = 500;
			document.body.appendChild(loading);
			await loading.present();	
		}
	}

  private async onRouteDidChange() {
		this.pageLoaded = true;
		const loading = document.getElementsByTagName('ion-loading');
		try {
			for (let i =0 ; i < loading.length; i++) {
				loading[i].dismiss();
			}
		} catch (e) {
			console.debug('route change remove loading', e);
		}
	}

  render() {
    return (
      <ion-app>
				<genie-stripe></genie-stripe>
        <ion-router
          onIonRouteWillChange={this.onRouteWillChange}
          onIonRouteDidChange={this.onRouteDidChange}
          useHash={false}
        >
          <ion-route url="/" component="app-home" />
          <ion-route url="/contact" component="app-contact" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
