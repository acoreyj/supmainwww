import { Component, h } from "@stencil/core";
import { renderHeading } from "../../helpers/utils";
@Component({
  tag: "app-home"
})
export class AppHome {
  render() {
    return [
      renderHeading(),
      <ion-content class="ion-no-padding">
        <ion-grid>
          <ion-row>
            <ion-col class="logo-col" size="12" size-lg="4">
              <component-logo></component-logo>
							<ion-row>
              <ion-col size="12" size-lg="8" offset-lg="2">
                <ion-card color="primary">
                  <ion-card-content color="dark">
                    30+ Years of Quality Personalized Services in Eau Claire and
                    surrounding areas.
                  </ion-card-content>
                </ion-card>
              </ion-col>
							</ion-row>
            </ion-col>
            <ion-col size="12" size-lg="8">
              <ion-row>
                <ion-col size="12" size-lg="7">
                  <genie-card
										no-hover
                    heading="Management Services"
                    text="For over 30 years Superior Maintenance has provided professional maintenance and
										control of commercial Property"
                  >
                    <ul slot="end">
                      <li>Multi Use / Condo Developments</li>
                      <li>Rental Complexes</li>
                      <li>Office and Retail Business Properties</li>
                    </ul>
                  </genie-card>
                </ion-col>
                <ion-col size="12" size-lg="5">
                  <genie-card
										no-hover
                    heading="Satisfaction Guaranteed"
                    text="We stress strong communication and guarantee your requirements will be met. Our team will provide efficient and professional results.  We come highly recommended, reach out to us for our history and references."
                  ></genie-card>
                </ion-col>
              </ion-row>

              <ion-row>
                <ion-col size="12" size-lg="7">
                  <genie-card
										no-hover
                    card-is-link="false"
                    href="/contact"
                    heading="Contact Us"
                  >
                    <ul>
                      <li>
                        Phone: <a href="tel:7153794872">715-379-4872</a>
                      </li>
                      <li>
                        Email:{" "}
                        <a href="mailto:superiormaintenance.jepperson@gmail.com">
                          <span class="nowrap">superiormaintenance</span>.<span class="nowrap">jepperson</span>@<span class="nowrap">gmail</span>.com
                        </a>
                      </li>
                      <li>
                        <ion-router-link href="/contact">
                          Contact Form
                        </ion-router-link>
                      </li>
                    </ul>
                  </genie-card>
                </ion-col>

                <ion-col size="12" size-lg="5">
                  <genie-card
										no-hover
                    heading="Custom Services"
                    text="Let us know what you need and we will create a custom plan that exceeds your expectations."
                  ></genie-card>
                </ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
        </ion-grid>
      </ion-content>
    ];
  }
}
