import { h } from "@stencil/core";

export function sayHello() {
  return Math.random() < 0.5 ? "Hello" : "Hola";
}

export function renderHeading() {
	const path = window.location.pathname;
  return (
    <ion-header class="ion-padding-start ion-padding-vertical">
      <ion-toolbar>
        <ion-router-link slot="start" href="/">
          <div class="logo">
            <img alt="Superior Maintenance Logo" src="/assets/logo.svg"></img>
          </div>
        </ion-router-link>

        <ion-buttons slot="start">
          <ion-button class={{'active' : path==='/'}} color="light" href="/" fill="clear">
            HOME
          </ion-button>
          <ion-button class={{'active' : path==='/contact'}} color="light" href="/contact" fill="clear">
            CONTACT US
          </ion-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
  );
}
